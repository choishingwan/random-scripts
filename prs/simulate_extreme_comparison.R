library(magrittr)
library(data.table)
library(forcats)
get_quantile <- function(x, num.quant) {
    quant <- as.numeric(cut(
        x,
        breaks = unique(quantile(x, probs = seq(
            0, 1, 1 / num.quant
        ))),
        include.lowest = T
    ))
    return(quant)
}

get_bias <- function(input, portion) {
    x <- copy(input) %>%
        .[, Group := "Normal"] %>%
        .[Quantile > 100 - portion, Group := "Upper"] %>%
        .[Quantile < portion + 1, Group := "Lower"] %>%
        .[, Group := as.factor(Group)] %>%
        .[, Group := fct_relevel(Group, "Lower",  "Upper", "Normal")]
    extreme <- x[!Group %in% "Normal"] %>%
        glm(CC ~ Group, ., family = binomial) %>%
        summary %>%
        coef
    target <- row.names(extreme)
    extreme.result <- extreme[target %in% "GroupUpper", "Estimate"]
    compare.all <-
        x[, Group := fct_recode(Group, Lower = "Normal")] %>%
        .[, Group := fct_relevel(Group, "Lower")] %>%
        glm(CC ~ Group, ., family = binomial) %>%
        summary %>%
        coef
    target <- row.names(compare.all)
    compare.all.result <-
        compare.all[target %in% "GroupUpper", "Estimate"]
    return(data.table(
        Portion = portion,
        Extreme = extreme.result,
        All = compare.all.result
    ))
}

get_inflation <- function(herit, prevalence, n.sample) {
    geno <- rnorm(n.sample, mean = 0, sd = sqrt(herit))
    pheno <- geno + rnorm(n.sample, mean = 0, sd = sqrt(1 - herit))
    dat <-
        data.table(
            Pheno = pheno,
            Quantile = get_quantile(geno, 100),
            Geno = geno,
            CC = pheno > qnorm(prevalence, lower.tail = F)
        )
    
    res <- NULL
    for (i in seq(1, 20, 2)) {
        res <- rbind(res, get_bias(dat, i))
    }
    res[, Herit := herit]
    res[, Prevalence := prevalence]
    return(res)
}

result <- NULL
n.sample <- 100000
perm <- 100
for (i in 1:perm) {
    for (h in c(0.1, 0.2, 0.3, 0.4)) {
        for (p in c(0.01, 0.1, 0.2, 0.3)) {
            print(paste(h, p))
            temp <- get_inflation(h, p, n.sample)
            temp[, Perm := i]
            result <- rbind(result, temp)
        }
    }
}

result %>%
    .[, ratio := Extreme / All] %>%
    .[, Herit := as.factor(Herit)] %>%
    .[, .(Inflation = mean(ratio), SE = sd(ratio) / sqrt(.N)), by = c("Herit", "Portion", "Prevalence")] %>%
    ggplot(
        aes(
            x = Portion,
            y = Inflation,
            ymin = Inflation - SE,
            ymax = Inflation + SE,
            color = Herit,
            group = Herit,
            fill = Herit
        )
    ) +
    geom_point() +
    geom_line() +
    geom_ribbon(alpha = 0.5) +
    facet_wrap(~ Prevalence, scale = "free") +
    theme_classic() +
    labs(x = "Proportion of extreme used",
         y = "Inflation of OR when comparing extremes vs comparing with all",
         color = expression(paste("Simulated ", R ^ 2)),
         fill = expression(paste("Simulated ", R ^ 2))) +
    theme(
        axis.title = element_text(face = "bold", size = 16),
        axis.text = element_text(size = 14),
        strip.text = element_text(size = 14),
        legend.text = element_text(size = 14),
        legend.title = element_text(size = 16, face = "bold")
    ) +
    scale_color_npg()
ggsave("Inflation.png", height = 10, width = 10)
